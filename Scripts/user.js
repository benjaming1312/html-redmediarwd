    // $(document).ready(function(){
    // var swiper = new Swiper('.section1 .swiper-container', {
    //     pagination: '.swiper-pagination',
    //     effect: 'coverflow',
    //     grabCursor: false,
    //     centeredSlides: true,
    //     slidesPerView:"auto",
    //     loop:true,
    //     autoplay: 1000,
    //     coverflow: {
    //         rotate: 50,
    //         stretch: -50,
    //         depth: 100,
    //         modifier: 1,
    //         slideShadows : false,
    //     }
    // });

    // });

    $(document).ready(function(){
     var swiper = new Swiper('.vertical .swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        direction: 'vertical',
        loop:true,
        autoplay: 3500,
    });
    });

    $(document).ready(function(){
        $('.online-check.owl-carousel').owlCarousel({
        loop:true,
        margin:20,
        items:4,
        dots:false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                width:300
            },
            440:{
                margin:10,
                items:3,
            },
            992:{
                items:4,
            }
        }
    })
});

// if ($(window).width() > 768) {
    //往下滑動增加nav
    $(function(){  
      $(window).scroll(function(){
        //var $(window).scrollTop(); 為 scroll
        var scroll = $(window).scrollTop();
        //當卷軸超過70px，自動加上 .navbar-fixed-top ，如果小於就移除
        if( scroll >= 130){      
          $(".navbar.navbar-default").addClass("navbar-fixed-top").slideDown();      
        }
        else{
          $(".navbar.navbar-default.navbar-fixed-top").removeClass("navbar-fixed-top");
        }    
      });
    })
// }    
// $(document).ready(function(){
    // $('.animated.bouce').hover(function(){
    //     $(this).toggleClass('tada')
    // })
    // $('.section3 .col-sm-6 .row .col-sm-6 a.btnuton').hover(function(){
    //     $(this).toggleClass('animated flash')
    // })
// });




//滾動animation
//當視窗高度到達物件時的動作
// function scrollanimation(a,b){
//         $(a).each(function() {
//       // Check the location of each desired element //
//       var objectBottom = $(this).offset().top + $(this).outerHeight();
//       var windowBottom = $(window).scrollTop() + $(window).innerHeight();
//       if (objectBottom < windowBottom) { //object comes into view (scrolling down)
//             $(this).addClass(b);
//         }
//     });
//         // If the element is completely within bounds of the window, fade it in //        
// };





//遇到class+animation
// $(window).on("load",function() {
//   $(window).scroll(function() {
//     var windowWidth =$(window).width();
//     if(windowWidth >768){
//     //複製以下新增
//         scrollanimation('.desire','animated fadeInUp');
//         scrollanimation('.rwdf #whyrwd','animated fadeIn');    
//         scrollanimation('.owl','animated fadeInUp');
//         scrollanimation('.rwd-case #rwdsys','animated fadeIn');
//         scrollanimation('.rwd','animated fadeIn');    
//         scrollanimation('.container.case .col-sm-4 .title','animated fadeIn');    
//         scrollanimation('.container.case .col-sm-4 .fa','animated fadeIn');
//         scrollanimation('.app .title','animated fadeIn');
//         scrollanimation('.app .fa','animated fadeIn');
//         scrollanimation('.fb-fans .row:nth-of-type(2)','animated fadeIn');
//         scrollanimation('.fb-fans .row:nth-of-type(3)','animated fadeIn');
//         scrollanimation('.fb-fans .row:nth-of-type(4)','animated fadeIn');
//         scrollanimation('.fb-fans .row:nth-of-type(5)','animated fadeIn');
//         scrollanimation('.fb-fans .row:nth-of-type(6)','animated fadeIn');
//         scrollanimation('.fb-fans .row:nth-of-type(7)','animated fadeIn');
//         scrollanimation('.blog-markting img:nth-of-type(2)','animated fadeIn');
//         scrollanimation('.blog-markting img:nth-of-type(3)','animated fadeIn');
//         scrollanimation('.wom img:nth-of-type(1)','animated fadeIn');
//         scrollanimation('.wom img:nth-of-type(2)','animated fadeIn');
//         scrollanimation('.case1','animated fadeInUp');
//         scrollanimation('.case2','animated fadeInUp');
//         scrollanimation('.media-marketing .col-xs-12:nth-of-type(1)','animated fadeIn');
//         scrollanimation('.media-marketing .col-xs-12:nth-of-type(2)','animated fadeIn');
//         scrollanimation('.media-marketing .col-xs-12:nth-of-type(3)','animated fadeIn')
//     }else{
//         $('#demo').removeClass('in');
//         $('#demo2').removeClass('in');
//         $('#demo3').removeClass('in');
//     }
//   }); 
// }); 
$(document).ready(function(){
    if ($(window).width() < 767) {
        var hasclassname = $('.container-fluid').hasClass('webdesigncase');
        if(hasclassname){
            $('.mbleft').addClass('showMenu');   
        }
        $('.menuappend').append($('.webdesigncase .container .row .col-sm-3'))
        $('.webdesigncase .container .row .col-sm-9').before($('.mbleft'))
        $('.showMenu span').click(function(){
            $('.showMenu').toggleClass('slide')
        })
        
    }
})
